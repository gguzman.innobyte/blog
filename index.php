<!doctype html>
<html lang="en">
<?php
    require_once("model/blog.php");
    session_start();
?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">MOMO'S BLOG</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Inicio</a>
                    </li>
                </ul>
                <form class="d-flex">
                <?php
                    if(isset($_SESSION["id"])){
                        echo('
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active">Bienvenid@ '. $_SESSION["fullname"] .'</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="controller/logout.php">Salir</a>
                                </li>
                            </ul>
                        ');
                    }else{
                        echo('
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="views/login.php">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="views/register.php">Registro</a>
                                </li>
                            </ul>
                        ');
                    }
                ?>
                </form>
            </div>
        </div>
    </nav>
</head>

<body>
    <!-- Creacion de blog -->
    <?php
        if(isset($_SESSION["id"])){
            echo('
                <div class="card bg-light mb-3" style="max-width: 80rem; margin:auto;">
                    <div class="card-body">
                        <form method="post">
                            <h5 class="card-title">Que tal: '. $_SESSION["fullname"] .' .! En que estas pensando?</h5>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Titulo</span>
                                </div>
                                <input aria-label="Small" type="text" class="form-control" name="name" aria-describedby="basic-addon1">
                                <button type="submit" name="addBlog" class="btn btn-dark">Publicar</button>
                            </div>
                            <div class="input-group">
                                <textarea aria-label="Small" class="form-control" name="description" aria-label="With textarea"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            ');
            include_once("controller/blog.php");
        }
    ?>
    <div class="p-12">
        <?php
            $blog = new Blog();
            $blogs = $blog->getNewBlogs();
            if(count($blogs) > 0){
                for($i = 0 ; $i < count($blogs) ; $i++){
                    $html = '';
                    $html .= '<div class="card text-white bg-dark mb-3" style="max-width: 60rem; margin:auto;">';
                    $html .= '<div class="card-header">BY: '. $blogs[$i]["fullname"];
                    if(isset($_SESSION["id"]) && $_SESSION["id"] == $blogs[$i]["author_id"]){
                        $html .= '<form method="post">';
                        $html .= '    <input name="id" value="'. $blogs[$i]["id"] .'" style="display:none">';
                        $html .= '    <a id="deletePublication'. $blogs[$i]["id"] .'" class="btn btn-danger float-sm-right" style="margin-top:-25px; margin-right:-10px">';
                        $html .= '        <span aria-hidden="true">&times;</span>';
                        $html .= '    </a>';
    
                        $html .= '    <button type="submint" id="submintCancel'. $blogs[$i]["id"] .'" name="deleteBlog" style="display:none; margin:10px; margin-top:-25px; margin-right:-10px" class="btn btn-danger float-sm-right">';
                        $html .= '        Aceptar';
                        $html .= '    </button>';
                        $html .= '    <a id="cancelDelete'. $blogs[$i]["id"] .'" style="display:none; margin:10px; margin-top:-25px;" class="btn btn-secondary float-sm-right">';
                        $html .= '        Cancelar';
                        $html .= '    </a>';
    
                        $html .= '</form>';
                        $html .= '<script>';
    
                        $html .= '    $("#deletePublication'. $blogs[$i]["id"].'").click(function(){';
                        $html .= '        $("#cancelDelete'. $blogs[$i]["id"].'").show();';
                        $html .= '        $("#submintCancel'. $blogs[$i]["id"].'").show();';
                        $html .= '        $("#deletePublication'. $blogs[$i]["id"].'").hide();';
                        $html .= '    });';
                        $html .= '    $("#cancelDelete'. $blogs[$i]["id"].'").click(function(){';
                        $html .= '        $("#cancelDelete'. $blogs[$i]["id"].'").hide();';
                        $html .= '        $("#submintCancel'. $blogs[$i]["id"].'").hide();';
                        $html .= '        $("#deletePublication'. $blogs[$i]["id"].'").show();';
                        $html .= '    });';
                        $html .= '</script>';
                    }
                    $html .= '</div>';
                    $html .= '    <div class="card-body">';
                    $html .= '        <h5 class="card-title">'. $blogs[$i]["name"] .'</h5>';
                    $html .= '        <p class="card-text">'. $blogs[$i]["description"] .'</p>';
                    for ($e=0; $e < count($blogs[$i]["comments"]) ; $e++) { 
                        $html .='    <div class="card bg-secondary  mb-3" style="">';
                        $html .='        <div class="card-header">by: ' . $blogs[$i]["comments"][$e]["fullname"] . '</div>';
                        $html .='        <div class="card-body">';
                        $html .='            <p class="card-text">'. $blogs[$i]["comments"][$e]["description"] .'</p>';
                        $html .='        </div>';
                        $html .='    </div>';
                    }
                    if(isset($_SESSION["id"])){
                        $html .='            <form method="post">';
                        $html .='          <div class="input-group mb-3">';
                        $html .='                <div class="input-group-prepend">';
                        $html .='                    <span class="input-group-text" id="basic-addon1">Comentario</span>';
                        $html .='                </div>';
                        $html .='                <input required aria-label="Small" type="text" class="form-control" name="description" aria-describedby="basic-addon1">';
                        $html .='                <input style="display:none" type="text" class="form-control" value="'. $blogs[$i]["id"] .'" name="blog_id" aria-describedby="basic-addon1">';
                        $html .='                <button type="submit" name="addComment" class="btn btn-dark">Publicar</button>';
                        $html .='          </div>';
                        $html .='            </form>';
                    }
                    $html .= '    </div>';
                    $html .= '</div>';
                    echo($html);
                    include_once("controller/blog.php");
                }
            }else{
                echo('
                <div class="col-12 text-center">
                    <h3>Aun no hay nada nuevo, se el primero en publicar!</h3>
                    '.(isset($_SESSION["id"]) ? '' : '<p>Inicia sesión/Registrate para continuar</p>').'
                </div>
                ');
            }
        ?>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>
    
</body>
</html>