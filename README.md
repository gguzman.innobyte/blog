# BLOG

Ejemplo de blog, desarrollado en PHP, HTML, MYSQL y JS

## Versiones

PHP 8.0.2

MYSQL 10.4.17-MariaDB

HTML 5

## Instalación

1- Crear base de datos

- En la carpeta "_db" se encuentra el modelo de base de datos en formato ".mwb"
- En caso de que no se cuente con [workbench](https://www.mysql.com/products/workbench/) se deja la base de datos en formato .sql.

2- Pasar la carpeta blog/ a la carpeta "htdocs" de apache

3- Dentro de blog/server/connection.php se encuentra la configuración de base de datos, hay que configurar el usuario y contraseñe en caso de ser necesarios



## Created by
[Guillermo gzt](https://www.linkedin.com/in/guillermo-guzman-torres-978492193/)
