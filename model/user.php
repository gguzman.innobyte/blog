<?php
    require("../server/connection.php");
    require("../controller/encrip.php");
    class User{
        private $db;

        public function __construct(){
            $this->db=connectionMysql::connection();
        }

        public function get_personas(){
            $response=$this->db->query("select * from user;");
            $user = [];
            while($filas=$consulta->fetch_assoc()){
                $user[]=$filas;
            }
            return $user;
        }
        public function insertUser($params){
            $result = array("success" => false, "message" => "");
            $existUser = $this->getUserByEmail($params["email"]);
            if(count($existUser) > 0){
                $result["message"] = "El correo ya esta registrado";
            }else{
                $encrip = new Password();
                $password = $encrip->hash($params["password"]);
                $response=$this->db->query("INSERT INTO user (fullname, email, password) VALUES ('". $params["fullname"] ."', '". $params["email"] ."', '". $password ."')");
                if (!$response) {
                    printf("Errormessage: %s\n", $this->db->error);
                }else{
                    $result["success"] = true;
                }
            }
            return $result;
        }
        public function login($params){
            $result = array("success" => false);
            $existUser = $this->getUserByEmail($params["email"]);
            if(count($existUser) > 0){
                $encrip = new Password();
                $valid = $encrip->verify($params["password"], $existUser[0]["password"]);
                if($valid){
                    session_start();
                    $result["success"] = true;
                    $_SESSION["fullname"] = $existUser[0]["fullname"];
                    $_SESSION["email"] = $existUser[0]["email"];
                    $_SESSION["id"] = $existUser[0]["id"];
                    header("Location: ../index.php");
                }else{
                    $result["message"] = "El usuario o la contraseña no se encuentran en el sistema (code: 001)";
                }
            }else{
                $result["message"] = "El usuario o la contraseña no se encuentran en el sistema (code: 002)";
            }
            return $result;
        }
        public function getUserByEmail($email){
            $response=$this->db->query("SELECT * FROM user WHERE email = '". $email ."'");
            $user = [];
            while($filas=$response->fetch_assoc()){
                $user[]=$filas;
            }
            return $user;
        }
    }
?>
