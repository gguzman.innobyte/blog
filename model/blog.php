<?php
    require_once("server/connection.php");
    class Blog {
        private $db;

        public function __construct(){
            $this->db=connectionMysql::connection();
        }

        public function getNewBlogs() {
            $response=$this->db->query("SELECT b.*, u.fullname FROM blog b INNER JOIN user u ON u.id = b.author_id WHERE b.disable = 1 ORDER BY b.id desc LIMIT 20;");
            $blogs = [];
            while($filas=$response->fetch_assoc()){
                $blogs[]=$filas;
            }
            for ($i=0; $i < count($blogs) ; $i++) { 
                $comments = $this->getCommentsBlog(array("id" => $blogs[$i]["id"]));
                $blogs[$i]["comments"] = $comments;
            }
            return $blogs;
        }
        public function getCommentsBlog($params) {
            $response=$this->db->query("SELECT c.description, u.fullname FROM commentary c INNER JOIN user u ON u.id = c.user_id WHERE blog_id = ". $params["id"] ."");
            $comments = [];
            while($filas=$response->fetch_assoc()){
                $comments[]=$filas;
            }
            return $comments;
        }
        public function insertCommentary($params) {
            $result = array("success" => false, "message"=>"");
            $response=$this->db->query("INSERT INTO commentary (description, blog_id, user_id) VALUES ('" . $params["description"] . "', '" . $params["blog_id"] . "', '" . $params["user_id"] . "')");
            if (!$response) {
                printf("Errormessage: %s\n", $this->db->error);
            }else{
                $result["success"] = true;
            }
            return $result;
        }
        public function insertBlog($params) {
            $result = array("success" => false, "message"=>"");
            $response=$this->db->query("INSERT INTO blog (name, description, author_id) VALUES ('" . $params["name"] . "', '" . $params["description"] . "', '" . $_SESSION["id"] . "')");
            if (!$response) {
                printf("Errormessage: %s\n", $this->db->error);
            }else{
                $result["success"] = true;
            }
            return $result;
        }
        public function deleteBlog($params) {
            $result = array("success" => false, "message"=>"");
            $response=$this->db->query("UPDATE blog SET disable = 0 WHERE id = ". $params["id"] .";");
            if (!$response) {
                printf("Errormessage: %s\n", $this->db->error);
            }else{
                $result["success"] = true;
            }
            return $result;
        }
    }
?>