<?php
    require_once("../model/user.php");
    if(isset($_POST["register"])){
        $params = array(
            'email' => $_POST["email"],
            'password' => $_POST["password"]
        );
        if(strlen($params['email']) > 0 && strlen($params['password']) > 0){
            $user = new User();
            $rep = $user->login($params);
            if($rep["success"]){
                echo('
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                        <strong>Registro exitoso: </strong>Tu cuenta se a generado con exito, 
                        <a type="submit" href="login.php">Ingresa al portar para comenzar la aventura</a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ');
            }else{
                echo('
                    <div class="alert alert-error alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                        <strong>No se pudo registrar la cuenta: </strong> '. $rep["message"] .'
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ');
            }
        }else{
            echo('
                <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                    <strong>Campos incompletos: </strong>Verifica la información, todos los campos deben estar completados
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ');
        }
    }
?>