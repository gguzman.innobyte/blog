<?php
    require_once("model/blog.php");
    if(isset($_POST["addBlog"])){
        $params = array(
            'name' => $_POST["name"],
            'description' => $_POST["description"]
        );
        if(strlen($params['name']) > 0 && strlen($params['description']) > 0){
            $blog = new Blog();
            $rep = $blog->insertBlog($params);
            if($rep["success"]){
                header("Location: index.php");
            }else{
                echo('
                    <div class="alert alert-error alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                        <strong>No generar la publicación: </strong> '. $rep["message"] .'
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                ');
            }
        }else{
            echo('
                <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                    <strong>Campos incompletos: </strong>Ingresa un titulo y una descripcion para continuar
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ');
        }
    }
    if(isset($_POST["deleteBlog"])){
        $params = array(
            'id' => $_POST["id"]
        );
        $blog = new Blog();
        $rep = $blog->deleteBlog($params);
        if($rep["success"]){
            header("Location: index.php");
        }else{
            echo('
                <div class="alert alert-error alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                    <strong>No generar la publicación: </strong> '. $rep["message"] .'
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ');
        }
    }
    if(isset($_POST["addComment"])){
        $params = array(
            'description' => $_POST["description"],
            'user_id' => $_SESSION["id"],
            'blog_id' => $_POST["blog_id"]
        );
        $blog = new Blog();
        $rep = $blog->insertCommentary($params);
        if($rep["success"]){
            header("Location: index.php");
        }else{
            echo('
                <div class="alert alert-error alert-dismissible fade show" role="alert" style="width:400px; margin:auto" >
                    <strong>No generar la publicación: </strong> '. $rep["message"] .'
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ');
        }
    }
?>