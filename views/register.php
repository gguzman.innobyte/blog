<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="../index.php">BLOG'S</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="../index.php">Home</a>
                    </li>
                </ul>
                <form class="d-flex">
                <?php
                    if(isset($_SESSION["USER_ID"])){
                        echo('
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active">BIenvenid@ '. $_SESSION["fullname"] .'</a>
                                </li>
                            </ul>
                        ');
                    }else{
                        echo('
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="login.php">Login</a>
                                </li>
                            </ul>
                        ');
                    }
                ?>
                </form>
            </div>
        </div>
    </nav>
</head>

<body class="">
    <div class="card d-flex justify-content-center bd-highlight mb-3 text-center" style="width:300px; padding:10px; margin: auto;">
        <form method="post" class="p-2 bd-highlight">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre completo</label>
                <input required type="text" class="form-control" name="fullname" aria-describedby="emailHelp" placeholder="Nombre">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input required type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Ingresa tu correo">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Contraseña</label>
                <input required type="password" class="form-control" name="password" placeholder="Contraseña">
            </div>
            <br>
            <button type="submit" id="adduser" name="register" class="btn btn-secondary">Registrar</button>
            <div style="display:none"  class="spinner-border text-dark" id="loader"></div>
            <br>
            <a type="submit" href="login.php">Login</a>
        </form>
    </div>
    <script>
        $("#adduser").click(function(){
            $("#loader").show();
            $("#adduser").hide();
        });
    </script>
</body>
<?php
    include("../controller/register.php");
?>
</html>